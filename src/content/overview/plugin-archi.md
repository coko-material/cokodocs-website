---
title: Plugin Architecture

class: "features"
part: 250
image: "CkDc-pluginArchitecture-BckgW.svg"
---

Extend CokoDocs word processor with our plugin framework. 
