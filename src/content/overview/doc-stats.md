---
title: Document Stats

class: "features"
part: 650
image: "CkDc-docStats.svg"
---

Word count, paragraph count, image count, table count etc stats are available with one click.
