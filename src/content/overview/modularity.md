---
title: Modular

class: "features"
part: 710
image: "CkDc-modularity.svg"
---

The underlying framework for CokoDocs is CokoServer - a modular framework for building publishing platforms.
