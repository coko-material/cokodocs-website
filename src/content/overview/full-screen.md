---
title: Full Screen

class: "features"
part: 500
image: "CkDc-Full screen-BckgW.svg"
---

Fullscreen functionality to minimise toolbar real estate is a useful feature of CokoDocs.
