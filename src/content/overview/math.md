---
title: Math

class: "features"
part: 250
image: "CkDc-Math-BckgW.svg"
---

Inline and block level math is supported. Author equations in CokoDocs.
