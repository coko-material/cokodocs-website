---
title: Developer Friendly

class: "features"
part: 200
image: "CkDc-devFriendly-BckgW.svg"
---

CokoDocs is vertically and horizontally extensible. We developed CokoDocs so others can innovate!
