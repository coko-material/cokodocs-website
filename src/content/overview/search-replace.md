---
title: Search and Replace

class: "features"
part: 600
image: "CkDc-searchReplace-BckgW.svg"
---

Search and replace support with replace single/replace all and ignore/apply caps is supported.

