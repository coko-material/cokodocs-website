---
title: Standards Compliant

class: "features"
part: 850
image: "CkDc-standardCompliant.svg"
---

CokoDocs uses HTML. We are moving to ODF compliant HTML document support.
