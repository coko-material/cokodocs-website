---
title: Keyboard Shortcuts

class: "features"
part: 550
image: "CkDc-KeyShortcuts-BckgW.svg"
---

Configurable keyboard shortcuts! (Can be configured per Operating System).

