---
title: CokoDocs
subtitle:  The Open Source Replacement for Google Docs and MS Word 365 
permalink: /
layout: home.njk
class: home
---
CokoDocs is an open source, collaborative, web-based Word Processor. 

![cokodoc gif demo](/images/cokodocs-sg2.gif){class="screengrab"}

[Try the beta](http://beta.cokodocs.net){.frontpage-button}


