---
title: Features
layout: single.njk
permalink: /features/
order: 20 
menu: "features"
class: "features"
---
{% featureTable  %}
{% featureTitle "Workflows" %}
{% feature "Notepad" %}
{% feature "Document Manager", "24q2" %}
{% feature "Workflow Configuration Manager", "24q2" %}
{% feature "Book Producer", "24q4" %}
{% feature "Website Manager", "24q4" %}

{% featureTitle "Collaboration" %}
{% feature "Concurrent Editing" %}
{% feature "Text Chat", "24q2" %}
{% feature "Video Chat", "24q2" %}

{% featureTitle "Editor Plugins" %}
{% feature "Math" %}
{% feature "Comment Threads (annotations)" %}
{% feature "Track Changes" %}
{% feature "Tables" %}
{% feature "Images" %}
{% feature "Links" %}
{% feature "Bi-directional Text Support" %}
{% feature "Undo/Redo" %}
{% feature "Semantic mMrkup" %}
{% feature "Keyboard Shortcuts" %}
{% feature "Numbered and Un-numbered Lists" %}
{% feature "Special Character Support" %}
{% feature "Super and Sub -script" %}
{% feature "Search and Replace" %}
{% feature "Bold, Italic, Strikethrough, Underline" %}
{% feature "Case Transformation" %}
{% feature "Fullscreen Display" %}
{% feature "Document Stats" %}
{% feature "Note Management" %}
{% feature "Code Blocks" %}
{% feature "Highlighting" %}
{% feature "ChatGPT (AI Assist v1)" %}
{% feature "ChatGPT (AI Assist v2)", "24q2" %}
{% feature "Asset Manager", "24q2" %}

{% featureTitle "Import/Export" %}
{% feature "Ingest MS Word/ODT", "24q2" %}
{% feature "Export PDF", "24q2" %}
{% feature "AI PDF Designer", "24q2" %}

{% featureTitle "Stack" %}
{% feature "Node" %}
{% feature "Graphql" %}
{% feature "Postgres" %}
{% feature "React" %}
{% feature "Express.js" %}
{% feature "CokoServer" %}

{% endfeatureTable %}
