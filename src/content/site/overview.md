---
title: Overview
layout: overview.njk
permalink: /overview/
order: 10
menu: "overview"
class: "features"
---

CokoDocs is an open source, collaborative, web-based Word Processor. We're developing CokoDocs to provide an extensible word processor that is a first-class citizen of the Web.

Today CokoDocs is a feature-rich notepad for instant collaboration online, and the project has plans to extend CokoDocs for use cases ranging from document management to book production and website management.

CokoDocs is a product of [Coko](https://coko.foundation), a not-for-profit that works to develop systems that help the publishing community deliver critical knowledge better, faster, and cheaper.
