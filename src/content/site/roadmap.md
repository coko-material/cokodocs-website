---
title: Roadmap
layout: single.njk
permalink: /roadmap/
order: 30 
menu: "roadmap"
class: "roadmap"
---

_Folders and files look like a lot of things..._

CokoDocs is a pioneering open-source, collaborative, web-based word processor designed to cater to a wide array of use cases. At its core, CokoDocs currently excels as a robust platform for collaborative document creation and editing. Yet, our vision extends far beyond its present capabilities. From its inception, CokoDocs has been engineered to support diverse workflows, enhancing flexibility and user experience.

<img src='/images/CokoDocsDiagram.jpg'>

## Workflows

Key initial workflows include:

* **NotePad Workflow**: Emulating the simplicity of platforms like Etherpad, this feature allows users to create and access documents through a unique, either user-generated or system-provided, path without the need for account creation or login. Document sharing is streamlined, requiring only the URL to be shared with collaborators. This approach prioritizes ease of use while maintaining security through obscurity.

* **Document Manager Workflow**: For users seeking more structured document management and enhanced security, CokoDocs offers a login-based model. After registering and signing in, users gain access to additional functionalities, including document sharing capabilities and a comprehensive file system for efficient organization of folders and files. This model is designed for both individual and organizational document management needs.

These foundational workflows mark just the beginning of CokoDocs' journey. Our roadmap is filled with ambitious plans to expand and refine the platform, ensuring it remains at the forefront of collaborative document editing solutions.

Expanding on the initial workflows supported by CokoDocs, we are exploring and developing functionalities that cater to a broader spectrum of use cases, further enhancing the versatility of our platform:

* **Website Manager Workflow**: Recognizing the parallels between the organizational structure of documents and website architecture, CokoDocs is designed to facilitate a directory structure that mirrors the layout found in content management systems (CMS) like WordPress, or static site generators such as Flax, Hugo and Eleventy (11ty). This workflow is ideal for web developers and content managers looking to streamline their site management process directly from a web-based word processor. It is also true that multiple sites could be managed form the one instance of CokoDocs.

* **Book Production Workflow**: With the inherent similarity between the hierarchical organization of files and folders in CokoDocs and the structural nesting found in books we will build a book producer workdlow. This feature is tailored for authors, editors, and publishers, enabling them to structure their manuscripts in a way that reflects the final book layout and then export directly to valid EPUB and beautiful looking PDF for print. 

These additional workflows underscore our commitment to broadening CokoDocs' applicability across various domains, ensuring it serves as a comprehensive tool for diverse content creation and management needs.

<h2>Forthcoming Features</h2>
We have the following to be completed by mid 2024:

* **AI Assistant**: An AI assistant for writing.

* **AI PDF Designer**: A design system for using natural language controls to design the fool and feel, and page structure, of PDF.

* **Document Ingestion Microservice**: For importing Word documents, focusing on <a href='https://xsweet.org'>XSweet</a> microservice integration.We will also use an existing open source tool (probably Pandoc) to convert ODT to HTML and conform to CokoDocs's internal HTML storage format as a microservice for importing ODT.

* **Export ODT**: Conversion of HTML to ODT using an existing open source tool and implementation as a microservice. 

* **Export PDF (pagedjs microservice)**: Integration of <a href='https://pagedjs.org'>Pagedjs</a> CLI microservice and implementation of side-by-side preview via Pagedjs browser render.

* **PDF Template Management**: Development of a template 'chooser', administration UI, template editor, save mechanism, and JavaScript hook integrations for bespoke plugins.

* **Asset Manager**: Building a front-end image management interface, including alt text management, image color profile analysis, image resolution display, and capabilities for image search, replace, delete, upload, batch upload, and insert caption.

* **Track Changes**: Implement, test and deploy CDRT (YJS) compliant track changes.

* **Code Blocks**: Implement, test and deploy CDRT (YJS) compliant code blocks.

* **Math Support**: Implement, test and deploy and deploying CDRT (YJS) compliant math blocks and inline inserts.

* **Text Chat**: Integration of text-based chat for asynchronous and synchronous communication, supporting diverse formatting and @mentions.

* **Video Chat (leveraging Jitsi)**: Integration with Jitsi services for time-based unique path connection.

* **Workflow Configuration Manager**: Building back-end configuration management service, front-end management interfaces, and adding initial configuration options.

<h2>Mocks</h2>

The following are mocks of the forthcoming file manager, AI Assistant, AI PDF Designer, and sharing screens.
<img src='/images/blank.png'>


Users will be able to CRUD folders...
<img src='/images/createfolder.png'>

...and also files...
<img src='/images/createdocument.png'>

Sharing will emulate standard model behaviour we all have seen in various systems.
<img src='/images/share.png'>

The AI asssistant is built and about to be integrated.
<img src='/images/ai.gif'>
