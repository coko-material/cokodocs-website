const axios = require('axios')
const Cache = require('@11ty/eleventy-cache-assets')
const markdownIt = require('markdown-it')
const markdownItAnchor = require('markdown-it-anchor')
const dotenv = require('dotenv')

// The options

module.exports = async function (eleventyConfig, options = {}) {
  dotenv.config()

  if (!options.token) {
    console.log(
      '[eleventy-plugin-gitlab-releases] no token was define. Using .env RELEASETOKEN'
    )
  }
  if (!process.env['RELEASETOKEN']) {
    console.log(
      '[eleventy-plugin-gitlab-releases] please define .env[RELEASETOKEN]'
    )
  }
  if (!options.gitlaburl) {
    throw new Error(
      '[eleventy-plugin-gitlab-releases] Please add the gitlaburl'
    )
  }
  if (!options.projectId) {
    throw new Error(
      '[eleventy-plugin-gitlab-releases] Please add the project ID from gitlab'
    )
  }
  if (!options.gitlabgroup) {
    throw new Error(
      '[eleventy-plugin-gitlab-releases] Please add the group the project is on gitlab'
    )
  }
  if (!options.gitlabproject) {
    throw new Error(
      '[eleventy-plugin-gitlab-releases] Please add the name of the project on gitlab'
    )
  }
  eleventyConfig.addShortcode('gitlabreleases', async () => {
    let output = ''
    let token
    if (options.token) {
      token = options.token
    } else {
      token = process.env['RELEASETOKEN']
        ? `PRIVATE-TOKEN ${process.env['RELEASETOKEN']}`
        : ''
    }

    const response = await axios
      .get(
        `${options.gitlaburl}/api/v4/projects/${options.projectId}/releases/`,
        {
          method: 'GET',
          mode: 'cors',
          headers: { token },
        }
      )
      .then((response) => {
        let urlregex = /\]\(\/uploads/g
        response.data.forEach((entry) => {
          entry.description = entry.description.replace(
            urlregex,
            `](${options.gitlaburl}/${options.gitlabgroup}/${options.gitlabproject}/uploads`
          )
        })

        for (const release of response.data) {
          let content = generateOutput(release)
          output = output + content
        }
        return output
      })
      .catch((error) => {
        console.error(error)
      })
    return output
  })
}

// return the content
// get links from the release
function getLinksFromRelease(release) {
  let links = ''
  for (link of release.assets.sources) {
    links =
      links +
      `<li><a href="${link.url}">Source code ${release.tag_name} (${link.format})</a></li>`
  }
  return links
}

function fulldate(value) {
  const options = {
    year: 'numeric',
    month: 'long',
    day: 'numeric',
  }
  const date = new Date(value)
  if (isNaN(date)) return value
  return Intl.DateTimeFormat('en-GB', options).format(date)
}

function generateOutput(release) {
  return `<section class="gitlab-release-entry">
    <div class="gitlab-release-date">${fulldate(release.created_at)}</div>
    <h2 id="">${release.name} (${release.tag_name})</h2>
    <div class="gitlab-release-links">
    <h3>Downloads</h3>
    <ul class="gitlab-release-downloadLinks">
    ${getLinksFromRelease(release)}
    </ul>
    </div>
    <div class="gitlab-release-content">${markdownify(release.description)}</div>
  </section>`
}

// markdown conversion for the output
let mdoptions = {
  html: true,
  linkify: true,
  typographer: true,
}

let mdit = markdownIt(mdoptions).use(markdownItAnchor, {})

// the markdownify
function markdownify(value) {
  if (!value) {
    return ``
  }
  return mdit.render(value)
}
