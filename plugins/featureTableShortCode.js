const markdownIt = require('markdown-it')
const markdownItAnchor = require('markdown-it-anchor')

// The options

module.exports = async function (eleventyConfig, options = {}) {
  eleventyConfig.addPairedShortcode('featureTable', function (content) {
    return `
    
<table class="featureTable">
  <thead>
		<tr>
			<th>Features</th>
			<th>Current</th>
			<th>Q1 2024</th>
			<th>Q2 2024</th>
			<th>Q3 2024</th>
			<th>Q4 2024</th>
		</tr>
    </thead>
	<tbody>${content}</tbody>
</table>
   `
  })

  eleventyConfig.addShortcode('featureTitle', function (content) {
    return `
	<tr class="headerRow">
			<th colspan="7">${content}</th>
		</tr>   `
  })

  eleventyConfig.addShortcode('feature', function (name, date) {
    return ` <tr>
			<td>${name}</td>
			<td>${!date ? "✓" : ""}</td>
			<td>${date == "24q1" ? "✓" : ""}</td>
			<td>${date == "24q2" ? "✓" : ""}</td>
			<td>${date == "24q3" ? "✓" : ""}</td>
			<td>${date == "24q4" ? "✓" : ""}</td>
		</tr>   `
  })
}
