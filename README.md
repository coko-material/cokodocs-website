The feature table

The table is between the feature table pair shortcode.

```
{% featureTable  %}

…

{% endfeatureTable %}
```

feature title creates a title with the name between quotes

```
{% featureTitle "Stack" %}
```

feature can then be added like this: 

current, you don’t add the timeframe, it will go to current by default.

otherwise: XXqY

where XX is the year (23,24, etc)
and Y the quarter


```
{% feature "node v.16" %}
```
Q2 2023

```
{% feature "graphql", "23q2" %}
```

Q1 2023

```
{% feature "dancing in the street", "23q1" %}
```

Q4 2023

```
{% feature "oy", "23q4" %}
```

